# Cyberwoven Webform

This modules provides a few cyberwoven-specific tweaks to the webform module. It is intended for D8 sites.

## Tweaks

1. Forcibly set each webform's "Allow users to post submissions from a dedicated URL" config value to false on webform entity update.
The purpose is to prevent anyone from submitting a webform unless its embedded/attached to a page or a block.

So for example, submissions would not be allowed under /form/contact.

This setting is found under Settings -> General -> URL PATH SETTINGS.

See cyberwoven_webform_webform_presave in cyberwoven_webform.module.
